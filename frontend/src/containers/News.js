import React, { useEffect, useState } from 'react';
import axios from 'axios';
import NewsForm from '../components/news/NewsForm';
import NewsList from '../components/news/NewsList';
import { Container } from '@mui/material';
import FileTransfer from '../mylib/FileTransfer';
import NewsUpdateDialog from '../components/news/NewsUpdateDialog';
import { useParams } from "react-router-dom";
import SeachBar from '../components/news/SeachBar';
import FreeSolo from '../components/news/FreeSolo';
const ServerEndPoint = "http://localhost:8000/api/";
const ServerFilesBasePath = "http://localhost:8000/";
function News() {
    const [title, setTitle] = useState(null);
    const [author, setAuthor] = useState(null);
    const [description, setDescription] = useState(null);
    const [media, setMedia] = useState();
    const [tempMedia, setTempMedia] = useState(null);
    const [list, setList] = useState([]);
    const [open, setOpen] = useState(false);
    const [selectedNews, setSelectedNews] = useState(null);
    const [updateMedia, setUpdateMedia] = useState();
    const params = useParams(); // useParamsı yeni bir örneğini oluşturdum.
    console.log(params._id, 'propsa gelen parametre');
    const getId = params._id;


    useEffect(() => {
        allNews();
    }, []);

    const selectMedia = (e) => {
        let file = e.target.files[0];
        let baseType = file.type.split('/')[0];
        console.log(baseType, file);
        setTempMedia(URL.createObjectURL(file));
        console.log(URL.createObjectURL(file))
        setMedia(e.target.files[0]);
    }

    const newsSave = (uploadedMedia) => {
        let data = {
            title,
            author,
            description,
        }

        if (uploadedMedia) {
            data.media = uploadedMedia
        }
        console.log(data);
        axios.post(ServerEndPoint + 'news/save', data).then(res => {
            if (res) {
                allNews();
                alert("Tebrikler! Yeni Haber Kaydı Başarılı!");
            }
        }).catch(err => {
            console.log(err);
        });

    }


    const handleNewsSubmit = () => {
        if (media) {
            FileTransfer(media, (filePath) => {
                console.log(filePath, 'end')
                let fileData = {
                    path: filePath,
                    type: media.type,
                    status: true
                }

                newsSave(fileData);

            });
        } else {
            newsSave();


        }

    }

    const allNews = () => {

        axios.get(ServerEndPoint + 'news/list').then(res => {
            console.log(res.data);
            setList(res.data);
        }).catch(err => {
            console.log(err);
        });

    }

    const removeNewsByName = (author) => {
        //alert(_id);
        let status = true;
        axios.delete(ServerEndPoint + "news/removeByName/" + author + '/' + status).then(res => {
            console.log(res);

            if (res.statusText === "OK") {
                allNews();
                alert("veriler silindi");
            }
        }).catch(err => {
            console.log(err);
        });
    }

    const removeNewsById = (_id) => {
        //alert(_id);
        let status = true;
        axios.delete(ServerEndPoint + "news/removeById/" + _id).then(res => {
            console.log(res);
            if (res.statusText === "OK") {
                allNews();
                alert("veriler silindi");
            }
        }).catch(err => {
            console.log(err);
        });
    }

    const newsItemSelected = (news) => {
        console.log("Guncelle Butonuna Basildi", news);
        setOpen(true);
        setSelectedNews(news);
       
    }
    const handleClose = () => {
        setOpen(false);
        setTempMedia(null);
        setTitle(null);
        setAuthor(null);
        setDescription(null);
        allNews();
    }
    const selectUpdateMedia = (e) => {
        let file = e.target.files[0];
        let baseType = file.type.split('/')[0];
        console.log(baseType, file);
        setTempMedia(URL.createObjectURL(file));
        console.log(URL.createObjectURL(file))
        setUpdateMedia(e.target.files[0]);
    }
    const newsUpdate = (getId) => {
        FileTransfer(updateMedia, (filePath) => {
            console.log(filePath, 'end')
            let fileData = {
                path: filePath,
                type: updateMedia.type,
                status: true
            }
            let data = {
                title: title,
                description: description,
                author: author,
            };
            if (fileData) {
                data.media = fileData
            }

            console.log(data);
            axios.put(ServerEndPoint + 'news/update/' + getId, data).then(res => {
                console.log(res);
            }).catch(err => {
                console.log(err)
            });
        });





    }


    return (
        <Container >
            <NewsForm
                setTitle={setTitle}
                setAuthor={setAuthor}
                setDescription={setDescription}
                handleNewsSubmit={handleNewsSubmit}
                selectMedia={selectMedia}
                tempMedia={tempMedia}
                setTempMedia={setTempMedia}
            />
            {
                list ? <FreeSolo list={list} /> : null
            }
            <br />
            {
                list ? <SeachBar list={list} /> : null
            }
            <NewsList
                list={list}
                removeNewsByName={removeNewsByName}
                removeNewsById={removeNewsById}
                newsItemSelected={newsItemSelected}
            />{
                selectedNews ?
                    <NewsUpdateDialog
                        open={open}
                        handleClose={handleClose}
                        selectedNews={selectedNews}
                        setTitle={setTitle}
                        setAuthor={setAuthor}
                        setDescription={setDescription}
                        newsUpdate={newsUpdate}
                        selectUpdateMedia={selectUpdateMedia}
                        tempMedia={tempMedia}
                        setTempMedia={setTempMedia}

                    />
                    : null


            }

        </Container>

    )
}
export default News;
