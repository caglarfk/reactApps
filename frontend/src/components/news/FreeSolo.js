import * as React from 'react';
import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';
import Autocomplete from '@mui/material/Autocomplete';

export default function FreeSolo({list}) {
  return (
    <Stack spacing={2} sx={{ width: 300 }}>
    
    <Autocomplete
  disablePortal
  id="combo-box-demo"
  options={list}
  getOptionLabel={option => option.title}
  sx={{ width: 300 }}
  renderInput={(params) => <TextField {...params} label="Data" />}
/>
      
    {/*      <Autocomplete
      id="combo-box-demo"
      options={list}
      getOptionLabel={option => option.title}
      style={{ width: 300 }}
      renderInput={params => (
        <div ref={params.InputProps.ref}>
          <label {...params.InputLabelProps}>Seach</label>
          <input {...params.inputProps} autoFocus />
        </div>
      )}
    /> */}
    </Stack>
  );
}