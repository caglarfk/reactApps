import TextField from '@mui/material/TextField';

import React ,{useEffect,useState} from "react";
function SeachBar({list}) {
    const [searchTerm, setSearchTerm] = useState("");
  console.log("list data",list.title)

    return(
<div>
<TextField
          id="filled-search"
          label="Search field"
          type="search"
          variant="filled"
          options={list}
          onChange={event=> {
            setSearchTerm(event.target.value);
          }}
          
        />
       
     {
         list.filter((val)=>{
             if(searchTerm===""){
                 return val
             }else if ((val.title||'').toLowerCase().includes(searchTerm.toLowerCase())){
                 return val
             }
         }).map((item,i)=>{
            return (
                <div  key={i}>
                    <p>{item.title}</p>
                </div>
            );
        })
     }
</div>
    );
      
}
export default SeachBar;
